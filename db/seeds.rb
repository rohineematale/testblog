# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.find_or_create_by_name({ :name => 'admin' }, :without_protection => true)
Role.find_or_create_by_name({ :name => 'moderator' }, :without_protection => true)
Role.find_or_create_by_name({ :name => 'user' }, :without_protection => true)
Role.find_or_create_by_name({ :name => 'guest' }, :without_protection => true)

user = User.find_or_create_by_email(:email => 'rohineematale@gmail.com', :password => 'rohinee123', :password_confirmation => 'rohinee123')
user.confirm!
user.add_role :admin