class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
    	t.string :title
    	t.text :description
    	t.integer :user_id
    	t.integer :tag_ids
      t.timestamps
    end
  end
end
