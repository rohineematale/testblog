The blog can have multiple posts. 
Posts will have multiple tags Each post can have multiple comments. 
User can comment only if he / she logs in via Facebook

Additional features of the Forum:

• Permalinks should be associated with each blog post (a permanent URL which will display a post and its comments). 

• All URLs must be RESTful and the URL for the post should contain the title of the post i.e http:////1-Hello-World. 

• All tags should be added by admin to a post by AJAX request 

• Tag names, associated both with post should be visible in a list on each page. Each tag should be clickable and lead the user to the corresponding posts with the same tag. 

• The blog administrator should have a separate login through which he can edit posts, delete unwanted comments and do general housekeeping.

