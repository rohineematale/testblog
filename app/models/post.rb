class Post < ActiveRecord::Base
	resourcify
  attr_accessible :title, :description
  has_many :comments, :dependent => :destroy
  has_many :tags ,:through => :post_tag_relations
  has_many :post_tag_relations
  scope :by_tag, lambda { |tag_name| joins(:tags).where("tags.name = ?", tag_name) }
	def to_param
    "#{id} #{title}".parameterize
  end
end