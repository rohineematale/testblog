class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :facebook_authorization, :only => [:create]
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(params[:comment])
    @comment.user_id = current_user.id
    respond_to do |format|
      if @comment.save
        current_user.add_role :user, @comment
        format.json{render :json => {:content=>@comment.content, :user=>User.find(@comment.user_id).email, :id=>@comment.id, :post_id=>@post.id } }
      else
        format.html { redirect_to post_path(@post) }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @comment = Comment.find(params[:id])
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to post_path(params[:post_id]) }
      format.json { head :no_content }
    end
  end

  def facebook_authorization
    if current_user.present?
      if current_user.provider.nil?
        binding.pry
        redirect_to user_omniauth_authorize_path(:facebook), :notice => "You need to logged in through Facebook to comment"
      end
    end
  end
end
