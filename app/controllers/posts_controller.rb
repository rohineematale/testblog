class PostsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  def index
    if params[:tag_name].present?
      @posts = Post.by_tag(params[:tag_name])
    else
      @posts = Post.all
    end
    @tags = Tag.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  def show
    @post = Post.find(params[:id])
    @comments = @post.comments
    @comment = @post.comments.build
    @tag = @post.tags.build
    @tags = @post.tags
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  def new
    @post = Post.new
    @tags = Tag.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  def edit
    @post = Post.find(params[:id])
    @tags = Tag.all
  end

  def create
    @post = current_user.posts.build(params[:post]) 
    respond_to do |format|
      if @post.save
        current_user.add_role :moderator, @post
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    tags = params[:post][:tags].split(',')
    tags.each{|t| t.strip! }
    params[:post].delete(:tags)
    @post = current_user.posts.find(params[:id])    
    tags.each do |t|  
      @post.tags += [Tag.find_or_create_by_name(t)]
    end  
    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end

end
