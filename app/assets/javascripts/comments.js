$(document).ready(function(){
  $('#new_comment').bind('ajax:success',function(evt, data, status, xhr){
		$(".comment_listing").append(
			"<p><b>"+data.user+" says:</b><a href='/destroy?id="+data.id+"&amp;post_id="+data.post_id+"' class='pull-right danger' data-confirm='Are you sure?'' data-method='delete' rel='nofollow'>X</a></p>"+data.content+"<br/><br/><hr/>"
			);
		$("#comment").val("");
	}).bind("ajax:error",function(xhr, status, error) {
		if (status.status == 401 || status.status == 0)
		{
			alert("To add comment you need to login through FACEBOOK");
			window.location = '/users/auth/facebook'
		}
		else
		{
			alert(status.responseText)
		}
	});
});